       IDENTIFICATION DIVISION.
       PROGRAM-ID. DoCalc.
       AUTHOR. Michael Coughlan.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01 BrithDate.
         02 YearOfBirth.
           03 CenturyOB PIC 99.
           03 YearOB PIC 99.
         02 MountOfBrith PIC 99.
         02 DayOfBirth PIC 99.
       PROCEDURE DIVISION.
       CalculateResult.
           MOVE 19750315 TO BrithDate
           DISPLAY "Mount is = " MountOfBrith
           DISPLAY "Century of brith is = " CenturyOB
           DISPLAY "Year of birth is = " YearOfBirth
           DISPLAY DayOfBirth "/" MountOfBrith "/" YearOfBirth
           MOVE ZEROES TO YearOfBirth
           DISPLAY "Brith date = " BrithDate.
           STOP RUN.

