       IDENTIFICATION DIVISION.
       PROGRAM-ID. DoCalc.
       AUTHOR. Michael Coughlan.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 FristNum PIC 9 VALUE ZEROS.
       01 SecondNum PIC 9 VALUE ZEROES.
       01 CalcResult PIC 99 VALUE 0.
       01 UserPrompt PIC X(38) VALUE 
       "Plase enter two single digit numbers".
       PROCEDURE DIVISION.
       CalculateResult.
           DISPLAY UserPrompt 
           ACCEPT FristNum 
           ACCEPT SecondNum 
           COMPUTE CalcResult = FristNum + SecondNum
           DISPLAY "Result is = ", CalcResult 
           STOP RUN. 